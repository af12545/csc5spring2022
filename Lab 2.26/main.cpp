/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on February 26, 2022, 8:46 AM
 */

#include <iostream> //the location of the library 

using namespace std;


int main(int argc, char** argv) {
//all code goes in this area (for now) 
    /*this allows you to use 
     full blocks and multiple lines! 
     so epic :D*/
    
    
 /*today we're going to get two numbers from the users,
  *  calculate the average of the two numbers
  * and then output the average to the user*/ 

    
    //declare two variables 
    //decide what kind of data I am storing 
    int num1;       //int stores whole numbers 
    int num2; 
    
    num1 =3.5; 
    cout << num1 << endl; //outputs the truncated number 
    
    //get data from the user 
    //Always prompt the user 
    cout << "Please enter two numbers: "; 
    
    cin >> num1 >> num2; //gets two numbers from the user 
    
    //bottom is good as well
    //cin >> num1;
    //cin >> num2; 
    
    double average = (num1 + num2) /2.0 ; //1 and 4 --> 2 
    
    //5/2 --> 2 because of integer vision 
    
    cout << "average: " << average << endl; 
    
    /* Get two numbers from the user. 
     * Output the numbers to the user
     * swap the numbers
     * output the numbers again to the user 
     * 
     * ex. user enters 5 3 
     * outupts 3 5 
     * 
     */
    cout <<"Please enter two numbers: "; 
    cin >> num1 >> num2; 
    
    cout << "num1: " << num1 << " num2: " << num2 << endl; 
    
    //we're creating a new temporary variable to store one of the numbers 
    int temp = num1; //create a copy of num1 
    num1 = num2;  //overwrite num1 with num2 value 
    num2 = temp; //use the copy of num1 (temp) and overwrite num2 with it 
    
    cout << "num1: " << num2 << " num2: " << num2 <<endl;  
    
    //other variable types 
    
    char c; 
    string text; 
    int x; //integers (whole numbers)
    float f; //stores decimals with less accuracy 
    double d; //stores decimals (high accuracy) 
    bool b; //stores true/false values; 
    
    
    return 0;
}

