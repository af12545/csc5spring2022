/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   Homework4_Main.cpp
 * Author: itchy
 *
 * Created on March 24, 2022, 4:29 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <fstream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "1 : First Program" << endl;
    cout << "2 : Secnond Program" << endl;
    cout << "3 : Third Program" << endl;


    int input;
    cin >> input;

    switch (input) {
        case 1:
        {
            cout << "Hey! We've developed an A.I. to play rock paper scissors. Or you can play yourself." << endl << "Input 1 for A.I" << endl << "Or 2 for self." << endl;
            int artificial;
            int number1;
            int number2;
            cin >> artificial;

            switch (artificial) {
                case 1: // Artificial Intelligence R.P.S 
                {
                    char confirm = 'N';
                    do {

                        cout << "Artificial Intelligence, Are you reaaaady?????" << endl;

                        int game = rand() % (3 - 1 + 1) + 1;
                        cout << game;


                        if (game == 1) {
                            cout << "AI entered Paper!" << endl;

                        } else if (game == 2) {
                            cout << "AI entered Rock!" << endl;

                        } else if (game == 3) {
                            cout << "AI entered Scissors!" << endl;
                        } else {
                            cout << "AI didn't enter that correctly! AI choices are R,P,and S. Try again if you choose." << endl;
                        }

                        cout << "Thank you AI 1. Now AI 2, enter your choice!" << endl;


                        int game2 = rand() % (3 - 1 + 1) + 1;
                        cout << game2;
                        if (game2 == 1) {
                            cout << "AI entered Paper!" << endl;

                        } else if (game2 == 2) {
                            cout << "AI entered Rock!" << endl;

                        } else if (game2 == 3) {
                            cout << "AI entered Scissors!" << endl;
                        } else {
                            cout << "AI didn't enter that correctly! AI choices are R,P,and S. Try again if you choose." << endl;
                        }

                        //Both users have inputted at this point. 

                        cout << "Excellent job AI! Let's see who won...." << endl << " AI 1 entered: " << game << endl;
                        cout << "AI 2 entered: " << game2 << endl << endl;
                        cout << "That means..." << endl;

                        if (((game == 1) && (game2 == 2)) || ((game2 == 1) && (game == 2))) {
                            cout << "Paper covered Rock! Paper wins!" << endl;
                        } else if (((game == 1) && (game2 == 3)) || ((game2 == 1) && (game == 3))) {
                            cout << "Rock breaks Scissors! Rock wins!" << endl;
                        } else if (((game == 2) && (game2 == 3)) || ((game2 == 2) && (game == 3))) {
                            cout << "Scissors cut paper! Scissors win!" << endl;
                        } else {
                            cout << "Aw... nobody wins..." << endl;
                        }

                        cout << "Play again? Enter Y for yes, or enter any other key for no." << endl;
                        cin >> confirm;
                    } while (confirm == 'Y' || confirm == 'y');
                    break;
                }
                case 2: // Problem from Homework 3 unchanged 
                {
                    string game;
                    string game2;
                    char confirm = 'N';
                    do {

                        cout << "It's time for Rock, Paper, Scissors! User 1, enter your sign! Enter P for paper, R for rock, or S for scissors." << endl;
                        cin >> game;

                        if (game == "P" || game == "p") {
                            cout << "You entered Paper!" << endl;

                        } else if (game == "R" || game == "r") {
                            cout << "You entered Rock!" << endl;

                        } else if (game == "S" || game == "s") {
                            cout << "You entered Scissors!" << endl;
                        } else {
                            cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
                        }

                        cout << "Thank you user 1. Now User 2, enter your choice!" << endl;
                        cin >> game2;


                        if (game2 == "P" || game2 == "p") {
                            cout << "You entered Paper!" << endl;

                        } else if (game2 == "R" || game2 == "r") {
                            cout << "You entered Rock!" << endl;

                        } else if (game2 == "S" || game2 == "s") {
                            cout << "You entered Scissors!" << endl;
                        } else {
                            cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
                        }

                        //Both users have inputted at this point. 

                        cout << "Excellent job! Let's see who won...." << endl << " User 1 entered: " << game << endl;
                        cout << "User 2 entered: " << game2 << endl << endl;
                        cout << "That means..." << endl;

                        if (((game == "P" || game == "p") && (game2 == "R" || game2 == "r")) || ((game2 == "P" || game2 == "p") && (game == "R" || game == "r"))) {
                            cout << "Paper covered Rock! Paper wins!" << endl;
                        } else if (((game == "S" || game == "s") && (game2 == "R" || game2 == "r")) || ((game2 == "S" || game2 == "s") && (game == "R" || game == "r"))) {
                            cout << "Rock breaks Scissors! Rock wins!" << endl;
                        } else if (((game == "S" || game == "s") && (game2 == "P" || game2 == "p")) || ((game2 == "S" || game2 == "s") && (game == "P" || game == "p"))) {
                            cout << "Scissors cut paper! Scissors win!" << endl;
                        } else {
                            cout << "Aw... nobody wins..." << endl;
                        }

                        cout << "Play again? Enter Y for yes, or enter any other key for no." << endl;
                        cin >> confirm;
                    } while (confirm == 'Y' || confirm == 'y');
                    break;

                }

            }

        }
        case 2: // Problem 3 and 7. Problem 3 questions are commented out but are functional. 
        {
            double liter;
            double liter2;
            double gallons;
            double gallons2;
            double miles;
            double miles2;
            double mpg;
            double mpg2;
            char confirm = 'N';

            do {
                ofstream fout;
                fout.open("data.dat");
                fout << "45" << endl;
                fout << "168" << endl;
                fout << "54" << endl;
                fout << "345" << endl;
                fout.close();

                ifstream fin;
                fin.open("data.dat");


                string info = "data.dat";
                {
                    ifstream fin(info);
                    fin >> liter;
                    fin >> miles;
                    fin >> liter2;
                    fin >> miles2;
                }

                cout << "The Liters stored on file for car 1 are: " << liter << endl;
                cout << "The miles stored on file for car 1 are: " << miles << endl;
                cout << "The Liters stored on file for car 2 are: " << liter2 << endl;
                cout << "The Miles stored on file for car 2 are: " << miles2 << endl << endl;



                // double liter;

                // cout << "Hello! Let's calculate how many liters you use, and how many miles you go! We will convert that to miles per gallon. " << endl;
                //cout << "First, enter how many liters you use." << endl;
                //  cin >> liter;
                //cout << "Perfect, now enter how many miles you travel." << endl;
                //  cin >> miles;

                gallons = liter * 0.264179;
                gallons2 = liter2 * 0.264179;

                mpg = miles / gallons;
                mpg2 = miles2 / gallons2;
                cout << "It looks like you have " << mpg << " miles per gallon!" << endl;
                cout << "For car 2, it looks like you have " << mpg2 << "miles per gallon!" << endl << endl;



                if (mpg > mpg2) {
                    cout << "Car 1 has better miles per gallon!" << endl;
                } else if (mpg2 > mpg) {
                    cout << "Car 2 has better miles per gallon!" << endl;
                } else {
                    cout << "Looks like both cars are the same mpg!" << endl;
                }


                cout << "Do you wish to calculate again? Enter Y for yes. Otherwise, enter any other key." << endl;

                cin >> confirm;
            } while (confirm == 'Y' || confirm == 'y');


            break;

        }
        case 3: //problem 5 
        {
            string phoneNum;
            string betterNum = "";

            cout << "Please Enter any sized phone number with one dash anywhere that is appropriate in the number." << endl;
            cin >> phoneNum;
            for (int i = 0; i < phoneNum.length(); i++)


                if (phoneNum[i] >= '0' && phoneNum[i] <= '9') {
                    betterNum = betterNum + phoneNum[i];
                }

            cout << betterNum;



            break;


        }
        case 4:
        {





        }
    }



    return 0;

}


