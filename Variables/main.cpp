/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on February 25, 2022, 12:47 PM
 */

#include <cstdlib>
#include <iostream>>
using namespace std;

//VARIABLES
//X + 2 = 37 wHAT IS x? x IS EQUAL TO 1
//x +10 =20? What is X? x is 10


//Data Types 
//the type of a variable 
//interger --> int (stores whole numbers) 
//decimal value -> double (twice the size of a float) 
//decimal value --> float 
// text (ascii characters) --> string 
// a single character --> char 
//true/false -> bool 
/*
 * 
 */
int main(int argc, char** argv) {
    
    //DELCARATIONS 
    //when we create a variable with a data type 
    //syntax: 
    //data_type variable_name;
    int x; //variable of x of type int 
    double d;
    string s; 
    bool b;
    char c;
    
    //cannot store a string in an integer variable 
    //need to make sure data types are matching 
   // x="hello"; NOT LEGAL because of syntax rules; does not match data types
    
    //INITIALIZING 
    // give a var. an initial value 
    //all variables contain junk when first declared 
    //use assignment operator (=) to initialize/assign a value 
    x = 10; //x is initialized to 10
    x=5;   // the variable x now stores the val. 5 
    
    d = 3.14;
    
    b = false; 

    c = 'c';  //c is assigned the character "c", use single quotes
            
    s = "Hello World";  //double quotes for a string 
    
    //Variable Names 
         //Syntax Rules; 
    //cannot start with numbers ex. int 2x; 
    //can start with underscores ex. int_2x; 
    //cannot use reserved words ex. int int; 
    //can use any combination of text and numbers 
    
    //style rules 
    //Do not start with a capitol letter ex. int X; 
    //DO use camelCase ex. int numberOfStudentsInClass; 
    //Try not to use underscores within var. name ex. int number_of_students_in_class;
    // Have good variable names ex. int n VS int numberOfCars;
    //Can NOT use symbols except for underscores 
    
    //CAN ONLY DECLARE A VARIABLE ONCE WITHIN THE SAME SCOPE 
    
    return 0;
}

