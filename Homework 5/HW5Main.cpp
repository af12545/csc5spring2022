/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   HW5Main.cpp
 * Author: itchy
 *
 * Created on March 31, 2022, 12:43 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>  
#include <fstream>
//For problem 6 on lbs+ounces conversion
using namespace std;

double inputPounds(double pounds) //declares parameters for pounds
{
    cin >> pounds; //takes input for pounds
    return pounds; //returns pounds to main
}

double inputOunces(double ounces) //declares parameters for ounces 
{
    cin >> ounces; //takes input for ounces
    return ounces; // returns ounces to main 
}

double convPounds(double pounds, double convKilograms, double kilograms) //declares paramerters for pounds, conversion value for kilograms, and a variable for the output to be stored in 
{
    kilograms = pounds * convKilograms; //converts the pounds using the conversion value to kilograms 
    return kilograms; //returns kilograms value to main function 
}

double convOunces(double ounces, double convGrams, double grams) //declares parameters for ounces, the conversion value for grams, and a variable for the output to be stored in 
{
    grams = ounces * convGrams; //converts to grams using the conversion value times ounces input 
    return grams; //returns gram value to main function
}

double finalPut(double kilograms, double grams) //declares parameters from the previous function for use in this function 
{
    cout << "You have " << kilograms << " kilograms and " << grams << "grams." << endl; //outputs a cout statement from the declared parameters, ending the program 
}
//For Problem 2 On Liters to Gallons 

double milesPerGallon(double miles, double gallons) //declares variables for liters and conversion
{
    double mpg = miles / gallons; //calculates gallons
    return mpg; // returns mpg to main function 
}

//Problem 3 on Customer Portions 

double customerPortion(double customers, double soylentOunces)
{
    double perPerson = soylentOunces / customers;
    return perPerson;
}

char confirm = 'N';

void AIRockPaper() //no parameters needed for this function
{
    do
    {
        cout << "Artificial Intelligence, Are you reaaaady?????" << endl;

        int game = rand() % (3 - 1 + 1) + 1;
        cout << game;


        if (game == 1)
        {
            cout << "AI entered Paper!" << endl;

        } else if (game == 2)
        {
            cout << "AI entered Rock!" << endl;

        } else if (game == 3)
        {
            cout << "AI entered Scissors!" << endl;
        } else
        {
            cout << "AI didn't enter that correctly! AI choices are R,P,and S. Try again if you choose." << endl;
        }

        cout << "Thank you AI 1. Now AI 2, enter your choice!" << endl;


        int game2 = rand() % (3 - 1 + 1) + 1;
        cout << game2;
        if (game2 == 1)
        {
            cout << "AI entered Paper!" << endl;

        } else if (game2 == 2)
        {
            cout << "AI entered Rock!" << endl;

        } else if (game2 == 3)
        {
            cout << "AI entered Scissors!" << endl;
        } else
        {
            cout << "AI didn't enter that correctly! AI choices are R,P,and S. Try again if you choose." << endl;
        }

        //Both users have inputted at this point. 

        cout << "Excellent job AI! Let's see who won...." << endl << " AI 1 entered: " << game << endl;
        cout << "AI 2 entered: " << game2 << endl << endl;
        cout << "That means..." << endl;

        if (((game == 1) && (game2 == 2)) || ((game2 == 1) && (game == 2)))
        {
            cout << "Paper covered Rock! Paper wins!" << endl;
        } else if (((game == 1) && (game2 == 3)) || ((game2 == 1) && (game == 3)))
        {
            cout << "Rock breaks Scissors! Rock wins!" << endl;
        } else if (((game == 2) && (game2 == 3)) || ((game2 == 2) && (game == 3)))
        {
            cout << "Scissors cut paper! Scissors win!" << endl;
        } else
        {
            cout << "Aw... nobody wins..." << endl;
        }

        cout << "Play again? Enter Y for yes, or enter any other key for no." << endl;
        cin >> confirm;
    } while (confirm == 'Y' || confirm == 'y');

}

void userRockPaper() //no parameters needed for this function 
{
    string game;
    string game2;
    char confirm = 'N';
    do
    {

        cout << "It's time for Rock, Paper, Scissors! User 1, enter your sign! Enter P for paper, R for rock, or S for scissors." << endl;
        cin >> game;

        if (game == "P" || game == "p")
        {
            cout << "You entered Paper!" << endl;

        } else if (game == "R" || game == "r")
        {
            cout << "You entered Rock!" << endl;

        } else if (game == "S" || game == "s")
        {
            cout << "You entered Scissors!" << endl;
        } else
        {
            cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
        }

        cout << "Thank you user 1. Now User 2, enter your choice!" << endl;
        cin >> game2;


        if (game2 == "P" || game2 == "p")
        {
            cout << "You entered Paper!" << endl;

        } else if (game2 == "R" || game2 == "r")
        {
            cout << "You entered Rock!" << endl;

        } else if (game2 == "S" || game2 == "s")
        {
            cout << "You entered Scissors!" << endl;
        } else
        {
            cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
        }

        //Both users have inputted at this point. 

        cout << "Excellent job! Let's see who won...." << endl << " User 1 entered: " << game << endl;
        cout << "User 2 entered: " << game2 << endl << endl;
        cout << "That means..." << endl;

        if (((game == "P" || game == "p") && (game2 == "R" || game2 == "r")) || ((game2 == "P" || game2 == "p") && (game == "R" || game == "r")))
        {
            cout << "Paper covered Rock! Paper wins!" << endl;
        } else if (((game == "S" || game == "s") && (game2 == "R" || game2 == "r")) || ((game2 == "S" || game2 == "s") && (game == "R" || game == "r")))
        {
            cout << "Rock breaks Scissors! Rock wins!" << endl;
        } else if (((game == "S" || game == "s") && (game2 == "P" || game2 == "p")) || ((game2 == "S" || game2 == "s") && (game == "P" || game == "p")))
        {
            cout << "Scissors cut paper! Scissors win!" << endl;
        } else
        {
            cout << "Aw... nobody wins..." << endl;
        }

        cout << "Play again? Enter Y for yes, or enter any other key for no." << endl;
        cin >> confirm; 
    } while (confirm == 'Y' || confirm == 'y');
}

/*
 * 
 */
int main(int argc, char** argv) //Main begins here! 
{

    cout << "Choose an option to start a program." << endl << endl;
    cout << "Option 1: Rock Paper Scissors" << endl << "Option 2: Liters To gallons" << endl << "Option 3: Ice Cream Portions" << endl << "Option 4: Unit Conversions.";

    int input;
    cin >> input;

    switch (input)
    {
        case 1:
        {
            cout << "Hey! We've developed an A.I. to play rock paper scissors. Or you can play yourself." << endl << "Input 1 for A.I" << endl << "Or 2 for self." << endl;
            int artificial;
            int number1;
            int number2;
            cin >> artificial;

            switch (artificial)
            {
                case 1: // Artificial Intelligence R.P.S 
                {
                    AIRockPaper();
                    break;
                }
                case 2:
                {
                    userRockPaper();
                }

                    break;
            }

            case 2:
            {
                double liter;
                double gallons;
                double miles;
                double mpg;
                const double conversion = 0.264179;
                char confirm = 'N';

                do
                {

                    double liter;

                    cout << "Hello! Let's calculate how many liters you use, and how many miles you go! We will convert that to miles per gallon. " << endl;
                    cout << "First, enter how many liters you use." << endl;
                    cin >> liter;
                    cout << "Perfect, now enter how many miles you travel." << endl;
                    cin >> miles;

                    gallons = liter * conversion;
                    cout << gallons;

                    cout << "It looks like you have " << milesPerGallon(miles, gallons) << " miles per gallon!" << endl; //function milesPerGallon is referenced on lines 22-26
                    cout << "Do you wish to calculate again? Enter Y for yes. Otherwise, enter any other key." << endl;
                    cin >> confirm;
                } while (confirm == 'Y' || confirm == 'y');


                break;
            }

            case 3:
            {
                const double soylentOunces = 256;
                double customers;
                double perPerson;
                cout << "Welcome to Soylent Green Ice Cream! We're pleased to see you all. As this limited edition ice cream is sparse, we will be portioning it out to all of you!" << endl;
                cout << "Please let us know how many people are here right now." << endl;

                cin >> customers;
                cout << "There are " << customerPortion(customers, soylentOunces) << "ounces of ice cream per person to share!" << endl;
                break;
            }
            
            case 4:
            {
                double pounds;
                double ounces;
                double convKilograms = 2.2046;
                double convGrams = 28.3495;
                double kilograms;
                double grams;
                cout << "We're going to be converting filthy imperial units to the superior metric system. Please input both a weight in pounds and ounces, and we'll do the rest." << endl;

                pounds = inputPounds(pounds);
                ounces = inputOunces(ounces);
                cout << "You entered " << pounds << " pounds and " << ounces << " ounces." << endl;

                kilograms = convPounds(convKilograms, pounds, kilograms);
                grams = convOunces(convGrams, ounces, grams);

                finalPut(kilograms, grams);
                break;
            }
        }
            return 0;
    }
}

