/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on February 25, 2022, 5:23 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    
    //Expression: 2 + 2; 
    
cout << 2+2 << endl; 

//order of operations 
// () parenthesis,  *(multiplication)  / (division), + (addition), - (subtraction) 
// ex 2 * 10 / 5 = 4
// ex 2* (10/5) = 4
// ex 2 * 10 + 2 = = 22
// ex 2 * (10 + 2) = 22
// ex 20 / (5+5) = 2 

int num1 = 20; 
int num2 = 5; 
int num3 = 5; 

cout << num1 /num2 + num3 <<endl; 

int num4 = num1 / num2 + num3; 

cout << num4 << endl;

// Interger division 
// 9/4 = 2.25 BUT everything after a decimial is truncated 
cout <<" 9/4 " << 9/4 << endl; 

// 11/4 = 2.75 EVERYTHING after the decimal is gone 

//do integer division, but store the results in a double variable 
//expression is evaluated first 

double val = 11/4; 
cout << "val: "  <<val <<endl; 

cout << "11/4: " <<11/4 << endl; 

    //to get the decimal value from division, use decimal division 
val = 11.0/4; 
cout << "val: " << val << endl; 

    // The modulus operator (%) 
    //The modulus operator returns the remainder of the integer division 

int mod1 = 11 % 4; 
cout << "11 % 4 " << mod1 <<endl; 

mod1 = 4 % 11; 
cout << "4 %11: " << mod1 <<endl; 
    

mod1 = 1000 % 20; 
cout <<"1000 % 20" << mod1 <<endl; 
    return 0;
}

