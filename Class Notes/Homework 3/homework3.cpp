/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   homework3.cpp
 * Author: itchy
 *
 * Created on March 12, 2022, 4:07 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "1 : First Program" << endl;
    cout << "2 : Secnond Program" << endl;
    cout << "3 : Third Program" << endl;
    cout << "4 : Fourth Program" << endl;

    int input;
    cin >> input;

    switch (input) {
        case 1:
        {
            //Problem 1 *****************************************************************
            int numbersInputted;
            int sumOfPos = 0;
            int sumOfNeg = 0;
            int sum = 0;
            int totalCnt, posCnt, negCnt;

            cout << "Please enter 10 numbers of your choosing. Actually nevermind. I'm generating that for you." << endl;
           
            for (int i = 0; i < 10; i++) {
             //   cin >> numbersInputted;
            numbersInputted = rand() % (100 - (-100) +1) + (-100); 
            cout << numbersInputted << endl; 
                if (numbersInputted >= 0) {
                    sumOfPos += numbersInputted;
                    posCnt++;
                } else {
                    sumOfNeg += numbersInputted;
                    negCnt++;
                }
            }
            totalCnt = sumOfPos + sumOfNeg;
            cout << "The total sum of positives are: " << sumOfPos << " The Average is: " << sumOfPos / posCnt << endl;
            cout << "the total sum of negatives are: " << sumOfNeg << "The Average is: " << sumOfNeg / negCnt << endl;
            cout << "The total is: " << totalCnt << " The average is: " << totalCnt / 10 << endl;

        }
            break;

        case 2:
        {
            //problem 2 *************************************************************************

            double weight;
            double height;
            double age;
            double bmr;
            double chocolate;
            char gender;
            char confirm;

            do {
                cout << "We are going to be calculating how many calories you need to survive purely through eating chocolate." << endl <<
                        "First you'll need to provide some sensitive personal information. Let's begin! Enter M if you are male, or F if you are female." << endl;
                cin >> gender;
                if (gender == 'M') {
                    cout << "You say you are a male. In that case, what is your weight in pounds?" << endl;
                    cin >> weight;
                    cout << "Nice. Enter your height in inches now." << endl;
                    cin >> height;
                    cout << "And finally, how old are you?" << endl;
                    cin >> age;
                    cout << "Excellent! Let's calculate how many chocolate bars you'll need..." << endl << "calculating..." << endl;

                    bmr = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
                    chocolate = bmr / 230;

                    cout << "You will need approximately " << chocolate << " chocolate bars to maintain your weight." << endl;
                } else if (gender == 'F') {
                    cout << "You say you are a female. In that case, what is your weight in pounds?" << endl;
                    cin >> weight;
                    cout << "Nice. Enter your height in inches now." << endl;
                    cin >> height;
                    cout << "And finally, how old are you?" << endl;
                    cin >> age;
                    cout << "Excellent! Let's calculate how many chocolate bars you'll need..." << endl << "calculating..." << endl;
                    bmr = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
                    chocolate = bmr / 230;

                    cout << "You will need approximately " << chocolate << " chocolate bars to maintain your weight." << endl;
                }
                else {
                    cout << "You missed that input actually. Please try using M for Male, and F for Female." << endl;
                }
                cout << "Enter Y if you want to use this program again. Otherwise, Press enter other key to exit." << endl;
                cin >> confirm;
            } while (confirm == 'Y');
        }
            cout << "Thank you for using this program!" << endl << endl;
            break;

        case 3:
            //problem 3 ************************************************************************************
        {
            double score = 0;
            double percent = 0;
            double total = 0;
            int n;
            double sum = 0;
            double indvScore;
            double indvTotal;
            cout << "How many exercises are we inputting today?" << endl;
            cin >> n;

            for (int i = 1; i <= n; i++) {
                cout << "What is the score for this exercise?" << endl;
                cin >> indvScore;

                cout << "What is the point value for this exercise?" << endl;
                cin >> indvTotal;

                score += indvScore;
                total += indvTotal;
                percent = (score / total) * 100;
            }
            cout << "Your total is " << score << " out of " << total << ", or ";
            cout << setprecision(4) << percent << "%.";
        }
            break;

        case 4:
            //problem 4 ****************************************************************************************************
        {
            string game;
            string game2;
            char confirm = 'N';
            do {

                cout << "It's time for Rock, Paper, Scissors! User 1, enter your sign! Enter P for paper, R for rock, or S for scissors." << endl;
                cin >> game;

                if (game == "P" || game == "p") {
                    cout << "You entered Paper!" << endl;

                } else if (game == "R" || game == "r") {
                    cout << "You entered Rock!" << endl;

                } else if (game == "S" || game == "s") {
                    cout << "You entered Scissors!" << endl;
                } else {
                    cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
                }

                cout << "Thank you user 1. Now User 2, enter your choice!" << endl;
                cin >> game2;


                if (game2 == "P" || game2 == "p") {
                    cout << "You entered Paper!" << endl;

                } else if (game2 == "R" || game2 == "r") {
                    cout << "You entered Rock!" << endl;

                } else if (game2 == "S" || game2 == "s") {
                    cout << "You entered Scissors!" << endl;
                } else {
                    cout << "You didn't enter that correctly! Your choices are R,P,and S. Try again if you choose." << endl;
                }

                //Both users have inputted at this point. 

                cout << "Excellent job! Let's see who won...." << endl << " User 1 entered: " << game << endl;
                cout << "User 2 entered: " << game2 << endl << endl;
                cout << "That means..." << endl;

                if (((game == "P" || game == "p") && (game2 == "R" || game2 == "r")) || ((game2 == "P" || game2 == "p") && (game == "R" || game == "r"))) {
                    cout << "Paper covered Rock! Paper wins!" << endl;
                }
                else if (((game == "S" || game == "s") && (game2 == "R" || game2 == "r")) || ((game2 == "S" || game2 == "s") && (game == "R" || game == "r"))) {
                    cout << "Rock breaks Scissors! Rock wins!" << endl;
                }                   
                else if (((game == "S" || game == "s") && (game2 == "P" || game2 == "p")) || ((game2 == "S" || game2 == "s") && (game == "P" || game == "p"))) {
                    cout << "Scissors cut paper! Scissors win!" << endl;
                }
                else {
                    cout << "Aw... nobody wins..." << endl;
                }

                cout << "Play again? Enter Y for yes, or enter any other key for no." << endl;
                cin >> confirm;
            }           
            while (confirm == 'Y' || confirm == 'y');
            break;
        }
          break;

        default:
            //if wrong input
            cout << "Try that again." << endl;
    }




    return 0;
}

