/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 4, 2022, 2:17 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //CONTROL FLOW
    //If statements
            // syntax: if (conditional_expression) 
                //conditional operators: 
                // < (less_than) 
                // > (greater_than) 
                // == (equal to) 
                // <= (less than or equal) 
                // >= (greater than or equal)
                // ! (not operator) ex !(condition) eg. !(x > 2) is NOT greater than 2 
     // All conditional expressions evaluate as true or false 
    
    int x = 10; 
                    // if the condition is true, then we execute code inside the curly braces
    if (x == 10)  //the conditino is x equal to 10
    {
    
        cout << "X is equal than 10" << endl; 
    }
    
    // Get some user input to have a grand ol time
    cout << "Please enter a number" << endl; 
    int input; 
    cin >> input ; 
    
    //use the else to get the other conditions 
    if (input > 10) 
    {
        cout << "x is greater than 10" << endl; 
    
    }
    else //otherwsie ALL the other conditions 
    {
        cout << "Input is not greater than 10" << endl; 
        cout << "proceeding with code..." << endl; 
    }
    
    //if the number is even or odd
    // using the modulus operator 
    // %
    //10 % 6 == 4 
    // 11 % 3 ==  2
    
    
    
    
    
    
    
    
    //if you need more than 2 conditions (as opposed to else being a catch all)
    //use the else if statement
    //the else if goes in between the if and the else of an if block
    
    cout << "Please enter another number: " << endl;
    cin >> input; 
    
    if (input > 10 )
    {
        cout << "Input is greater than 10" << endl ;
    }
    
    
    else  if (input < 10) //Always use an else when using an else if 
    {
        cout << "input is less than 10" << endl; 
    }
    else 
    {
         cout << "Input is equal to 10" << endl ; 
    }
   
    
    
    
    
    
    
    //all conditional expressions are evaluated as true or false 
    //only one condition can be output as true within an if block 
    cout << "Enter yet another number. "; 
    cin >> input; 
    
    if (input < 5 )
    {
        cout << "input is greater than 5" << endl; 
    }
    else if (input > 3 )
    {
        cout << "Input is greater than 0" << endl;
    
    }
    else if (input > 0)
    {
        cout << "Input is greater than 0" << endl;
        
        
    }
    
    else {
        cout << "input is less than 0 " << endl; 
    }
    
    
    
    
    
    
    //Nested if statements
    cout << "You know the drill: " ; 
    cin >> input ;
    
    if (input > 0)
    {
        
             if (input > 50)
             { cout << "input is greater than 50" << endl; 
        }
        else 
        {
                 cout << "Input is not greater than 50" << endl; 
        }
             
    }
    else 
    {
        if (input == 0) 
        {
        cout << "Input is equal to zero" << endl; 
        }
        else 
        {
            cout << "input is negative" << endl;  
        }
    }
    
    
    //compound boolean expression 
//more than 1 expression within a single statement 
    
    cout << "Please enter the final number!" << endl; 
    cin >> input ; 
    
    
    //Use && for AND
    //Use || (pipes) for OR 
    if (input > 0 && input <100 ) 
    {
        cout << "Input is within accepted range!" << endl; 
    }
    else if (input >= 100  || input > -100)
    {
        cout << "The number is too large!!!!!!!!" << endl; 
    }
    else if (input < 0)
    {
        cout << "The number is too small!!!!!!!!!!!!!!!!" << endl; 
    }
    else 
    {
        cout << "what kinda number is that supposed to be?" << endl; 
    }
    
    // When using && BOTH conditional expressions must be true
    //When using || Either conditional expression must be true 
    
    // ex. true && true = true
    //ex. true && false = false
    //both sides have to be true when using AND statements
    
    //true || false = true 
    //false || true = true
    // true || true = true
    // false || false = false
    //with OR statements, only one value needs to be true 
    
    /* 
     you can have compound statements such as
     
     true || false && true || false 
     
     * for || if first condition is true, everything is true 
     * this is called lazy evaluation 
     * 
     * true && false || true && false 
     * means true for the first two
     */
    
    //switch statements 
    cout << "enter a number for your switch statement " << endl ;
    cin >> input ; 
    
    // syntax(val to switch on) 
    // case val: 
    // code 
    // break; 
    
    switch ( input )
    {
        case 1:
        cout << "the value is 1" << endl; 
        break; 
        case 2:
        cout << "The value is 2" << endl; 
        break; 
        case 100:
        cout << "The value is 100" << endl; 
        break;
        default: // this is exactly like the else of an if case
            cout << "entered an invalid number" << endl; 
            
                
    }
    
    
    
    
    
    return 0;
}



