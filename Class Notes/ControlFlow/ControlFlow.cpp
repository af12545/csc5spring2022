/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   ControlFlow.cpp
 * Author: itchy
 *
 * Created on March 24, 2022, 1:18 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

void myFunction(); 

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    myFunction; 
    
    //string data type
    string s = "Hello World"; 
    
    //string is known as a class 
    // s is a variable that holds a string object 
    // an object is an instance of a class 
    // a class is a user defined data type 
    string s2 = "sup"; 
    
    //objects contain properties 
    //a string contains characters
    
    //objects contain member function 
    //a function is a specific task eg. srand(), rand() getline(cin, a) 
    //a member is what the function acts on
    //a member function only acts on that specific member
    //is used with the dot operator (.) 
    
    
    //string member functions 
    
    //length of a string
    // use .length() or .size() 
  //  s2.length(); 
    //can store the function in an integer or output it!
    int length = s2.length(); 
    cout << "length of s2 is " << length << endl; 
    //
    cout << "The Length of s is: " << s.length() << endl; 
    
    //not a function!
    // int x = (2+3) / 5; 
    
    //2nd member function 
    // .substr(int location)
    // .substr(int location, int cout) 
    // e.x. s.substr(0, 1); 
    //returns the substring of a string 
    
    // "Hello World" 
    // starts location at 0 because C++ is a zero based index language 
    // cout location starts at 1 
    // H ---> location 0
    // E ---> location 1 
    // L ---> location 2 ect. 
    
    // ex Get the substring "LLO" 
    // Start at location 2, get 3 total characters 
    
    cout << s.substr(2, 3) << endl; 
    
    // Find member function 
    // find any character or given substring from a string 
    //returns the location (index) of that string 
    // .find(string) 
    cout << s.find("o") << endl; 
    
    
    // SUBSCRIPTS 
    
    string name = "Bill Gates";
    
    //Location and Index (the same thing) 
    // name has 10 characters
    cout << "Name characters " << name.length() << endl; 
    
    // The largest index (location) is 9
    // This is because the index starts at 0 
   
    //use the subcript operator () 
    // a location is inside the [] ex. [0]
    
    // real example; name, [0] 
    //return the character at that location (would be "B")
    cout << "character at location 0: " << name[0] << endl; 
    
    cout << "2nd location: " << name[1] << endl; 
    
    // get character at location 20???
    cout << "20th location: " << name[20] << endl; 
    // bounds error since there is only 10 characters 
    
    
    // USING LOOPS AND STRINGS TOGETHER 
    
    //user enters string "Hello" 
    //user sees 
    //H
    //E
    //L
    //L
    //O
    
    cout << "Please enter a string" << endl; 
    string input; 
    getline (cin, input); 
    
    // we have the entire string 
    // we have to iterate across the entire string 
    // use the for loop and subscript operator 
    //use the for loop and subscript operator 
    // I know the size/length of the string, hence why you have a loop
    
    //start at index 0.loop "size" amount of times 
    for(int i = 0; i < input.size(); i++) 
    {
        // use subscript operator to evolve i 
        cout << input[i] << endl; 
    }
    
    // output the string in reverse!? 
    
    for (int i = input.size() - 1; i >= 0; i--)
    {
        cout << input[i] << endl; 
    }
    
    // output all characters in caps lock???
    for (int i = 0; i < input.length(); i++ )
    {
        char c = toupper(input[i]);
        cout << c; 
        
        //explicit conversion 
    cout << static_cast<char>(toupper(input[i])); 
    }
    
    
    
    
    return 0;
}

