/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   project1Main.cpp
 * Author: itchy
 *
 * Created on April 3, 2022, 9:40 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include<stdlib.h>

using namespace std;
//Functions Will begin here

void mainMenu() //This Function Displays the main menu. 
    {
    cout << "Choose a game!" << endl;
    cout << setfill('-') << setw(40) << "-" << endl;
    cout << "1. BlackJack" << endl;
    cout << "2. Craps (Not a bad word)" << endl;
    cout << "3. Insane Challenge...." << endl;
    } //nothing to return, hence is void 

int moneyPoolAdd(int money, int bet) //This function calculates the money won if the user wins 
    {
    bet = bet * 2;
    int moneyIncrement = money + bet;
    return moneyIncrement; //returns the money and the winner's earnings back to the pool 
    }

int moneyPoolSub(int money, int bet) //This function calculates the money lost if the user loses
    {
    int moneyDecrease = money - bet;
    return moneyDecrease; //returns decreased money from calculated wager back to the user's pool 
    }

int cardCalc(int randomizer) //declares randomizer that works for all card shuffles in blackjack 
    {
    char choice;

    randomizer = rand() % 13 + 1;

    if (randomizer == 11) //if statement intercepts randomized output and converts king, queen and joker into 10 
        {
        randomizer = 10;
        }
    else if (randomizer == 12)
        {
        randomizer = 10;
        }
    else if (randomizer == 13)
        {
        randomizer = 10;
        }
    else if (randomizer > 0 && randomizer < 2) // intercepts a value of 1 in order to convert a hard ace into a soft ace. 
        {
        cout << "an ace! Do you want to keep it as a 1 or change it to an 11? Enter Y if you want to keep it as is." << endl;
        cin >> choice;
        if (choice == 'Y' || choice == 'y')
            {
            cout << "You chose 1!" << endl;
            randomizer = 1;
            }
        else
            {
            cout << "You chose 11!" << endl;
            randomizer = 11;
            }
        }
    else
        {
        randomizer;
        }

    return randomizer; //returns randomized number back to the function 
    }

int cardCalcAI(int randomizer) //similarly, runs the AI for cards since it doesn't make sense to have the user choose if the house keeps an Ace. Still uses randomizer 
    {

    randomizer = rand() % 13 + 1;
    if (randomizer == 1)
        {
        randomizer = 11;
        }
    else if (randomizer == 11)
        {
        randomizer = 10;
        }
    else if (randomizer == 12)
        {
        randomizer = 10;
        }
    else if (randomizer == 13)
        {
        randomizer = 10;
        }

    return randomizer; // returns randomized variable to main function 
    }

//////////////////////////////////////////////////////////////////////functions will end here, main begins below /////////////////////////////////////////

int main(int argc, char** argv)
    {
    int money = 100; //money pool avaiable 
    int bet; //how much was bet 
    int game; //outcome 
    char confirm = 'n';
    cout << "Hello and Welcome to Moron-Go. Today our specials are on blackjack and craps!" << endl;
    do
        { //loops the game at least once and returns to this point if player wants to play again 
        cout << "Your funds are now " << money << endl;
        cout << setfill('-') << setw(40) << "-" << endl;
        mainMenu();

        int switch1;
        cin >> switch1;
        switch (switch1)
            {
            case 1: //Blackjack 
            { //beginning of blackjack
                srand(time(0)); //randomizes all randomized values 
                int aceVal;
                int houseHand1 = 0;
                int houseHand2 = 0;
                int playerHand1 = 0;
                int playerHand2 = 0;
                int houseHand3 = 0;
                int playerHand3 = 0;
                int randomizer;
                cout << "This is blackjack! The rules are simple, get the highest number of cards you can, while not passing 21! All the while the house will be tryint to beat your number... Ready?" << endl;
                cout << setfill('-') << setw(40) << "-" << endl;
                cout << "How much do you want to bet?" << endl;
                cin >> bet;
                if (bet < money)
                    {
                    ///////////////////////////////////// //This is where Blackjack will be calculated///////////////////////////////////////////////////////
                    char hit;

                    //house 1 here

                    // cardCalcAI(randomizer);
                    houseHand1 = cardCalcAI(randomizer);
                    cout << "House got " << houseHand1 << " and ";
                    //house 2 here 

                    // cardCalcAI(randomizer);
                    houseHand2 = cardCalcAI(randomizer);

                    cout << houseHand2 << "." << endl;

                    cout << "Player turn!" << endl << endl;

                    //player 1 here 
                    // cardCalc(randomizer);
                    playerHand1 = cardCalc(randomizer);

                    cout << "Player got " << playerHand1 << " and ";

                    //player 2 here  

                    playerHand2 = cardCalc(randomizer);

                    cout << playerHand2 << "." << endl;


                    ////////////////////////////////////////////////////////////////////End of first round //////////////////////////////////////////////////////
                    cout << endl << "Do you want to hit? Enter Y if you do." << endl;
                    cout << setfill('-') << setw(40) << "-" << endl;
                    cin >> hit;


                    if (hit == 'Y' || hit == 'y')
                        {
                        cardCalc(randomizer);
                        playerHand3 = cardCalc(randomizer);

                        cout << "You got " << playerHand3 << "!!!" << endl;

                        }
                    if (houseHand1 + houseHand2 <= 17)
                        {
                        cardCalc(randomizer);
                        houseHand3 = cardCalc(randomizer);
                        cout << "house got " << houseHand3 << "!!!" << endl;
                        }

                    int houseTotal = houseHand1 + houseHand2 + houseHand3;
                    int playerTotal = playerHand1 + playerHand2 + playerHand3;

                    if (playerTotal > 21)
                        {
                        cout << "You have " << playerTotal << " which is over 21!" << endl;
                        game = 0;
                        }
                    else if (houseTotal > 21)
                        {
                        cout << "House has " << houseTotal << " which is over 21..." << endl;
                        game = 1;
                        }
                    else
                        {
                        if (playerTotal > houseTotal)
                            {
                            cout << "You got a total of " << playerTotal << " while the house got " << houseTotal << endl << endl;
                            game = 1;
                            }
                        else
                            {
                            cout << "You got a total of " << playerTotal << " while the house got " << houseTotal << endl << endl;
                            game = 0;
                            }
                        }
                    }
                else
                    {
                    cout << "You're too broke! Bet a lesser amount, or, leave!" << endl;
                    game = 3;
                    }


                /* srand(time(0)); used this for testing 
                int game = rand() % 2;             
                 */
            }
                break;


                // end of blackjack 

            case 2: /////////////////////////////////////////////////////////////Craps///////////////////////////////////////////////////////////////
            { //Begnning of craps
                int choiceCraps;
                cout << "We offer two types of cr*ps here! Choose either a pass line bet or a don't pass wager." << endl;
                cout << "A pass line bet is won if the come out roll is 7 or 11. You lose if you get 2, 3 or 12!" << endl;
                cout << "A don't pass wager wins if the come out roll is 2 or 3, and loses if 7 or 11. If you get 12, the bet is doubled." << endl << endl;
                cout << "How much do you want to bet?" << endl;
                cout << setfill('-') << setw(40) << "-" << endl;
                cin >> bet;
                cout << "So which do you want to do? Enter 1 for pass line or 2 for don't pass wager" << endl << endl;
                cout << setfill('-') << setw(40) << "-" << endl;

                // Round 1

                cin >> choiceCraps;
                switch (choiceCraps)
                    {
                    
                    case 1:
                    {
                        srand(time(0)); 
                        int dice1 = rand() % 6 + 1; // 1-6
                        int dice2 = rand() % 6 + 1;
                        int roll1 = dice1 + dice2;

                        cout << "Rolling first round... " << roll1 << endl;

                        // Check if won/loss
                        if (roll1 == 7 || roll1 == 11)
                            {
                            game = 1;
                            }
                        else if (roll1 == 2 || roll1 == 3 || roll1 == 12)
                            {
                            game = 0;
                            }
                        else
                            {

                            // Round 2
                            dice1 = rand() % 6 + 1; // 1-6
                            dice2 = rand() % 6 + 1;
                            int roll2 = dice1 + dice2;

                            cout << "Roll 2: " << roll2 << endl;

                            while (roll2 != 7 && roll2 != roll1)
                                {
                                cout << "Rolling Again!" << endl;

                                dice1 = rand() % 6 + 1; // 1-6
                                dice2 = rand() % 6 + 1;
                                roll2 = dice1 + dice2;

                                cout << "Roll 2: " << roll2 << endl;
                                }

                            if (roll2 == 7)
                                {
                                game = 1; //roll of 7 wins in this version of bet 
                                }
                            else
                                {
                                game = 0;
                                }
                            }
                    }
                    case 2:
                    {
                        srand(time(0)); 
                        int dice1 = rand() % 6 + 1; // 1-6
                        int dice2 = rand() % 6 + 1;
                        int roll1 = dice1 + dice2;

                        cout << "Rolling first round... " << roll1 << endl;

                        // Check if won/loss
                        if (roll1 == 2 || roll1 == 3)
                            {
                            game = 1;
                            }
                        else if (roll1 == 7 || roll1 == 11)
                            {
                            game = 0;
                            }
                        else if (roll1 == 12)
                            {
                            //round 2
                            bet * 2;
                            dice1 = rand() % 6 + 1; // 1-6
                            dice2 = rand() % 6 + 1;
                            int roll2 = dice1 + dice2;

                            cout << "Roll 2: " << roll2 << endl;

                            while (roll2 != 7 && roll2 != 11)
                                {
                                cout << "Rolling Again!" << endl;

                                dice1 = rand() % 6 + 1; // 1-6
                                dice2 = rand() % 6 + 1;
                                roll2 = dice1 + dice2;

                                cout << "Final roll.... " << roll2 << endl;
                                }
                            }
                        else
                            {
                            // Continuation D
                            // Round 2
                            dice1 = rand() % 6 + 1; // 1-6
                            dice2 = rand() % 6 + 1;
                            int roll2 = dice1 + dice2;

                            cout << "Roll 2: " << roll2 << endl;

                            while (roll2 != 7 && roll2 != roll1)
                                {
                                cout << "Rolling Again!" << endl;

                                dice1 = rand() % 6 + 1; // 1-6
                                dice2 = rand() % 6 + 1;
                                roll2 = dice1 + dice2;

                                cout << "Final roll...: " << roll2 << endl;
                                }

                            if (roll2 == 7)
                                {
                                game = 0;
                                }
                            else
                                {
                                game = 1;
                                }
                            }

                    }
                        break;
                    }
                break;
            } // End of craps 
            case 3: // the challenge... 
                char accept;
                srand(time(0)); 
                cout << "You've messed up buddy. You've really done it now. You've accepted... the CHALLENGE. As a show of good faith... we'll let you leave right now. Accept?" << endl << "Y: SAVE YOURSELF";
                cout << endl << "N: DON'T DO IT" << endl;
                cin >> accept;

                if (accept == 'Y' || accept == 'y')
                    {
                    cout << "Wise choice..." << endl;
                    game == 3;
                    }
                else
                    {
                    cout << "Are you absolutely sure!?!?" << endl << "Y: RUUUUN " << endl << "N: ⤨⪏ⷌ⠐⾨✧Ⓓⴼ⛣ⷰ∌⼍⮣⣓⫞⣠⭙⛰ⱙ⥦₯₳₋⮱⡭‹ⴣ☽₁⨪⸆" << endl;
                    cin >> accept;
                    if (accept == 'Y' || accept == 'y')
                        {
                        cout << "Wise choice..." << endl;
                        game == 3;
                        }
                    else
                        {
                        cout << "You brought this upon yourself. Your challenge... a random amount of time will be set, while you stare at a symbol. If you blink, you will honor the ancient codes." << endl;
                        int start = clock();
                        double diff;
                        int time; 
                        time = rand() % 15 + 1; 
                         cout << "The time you're going to be staring is: " << time << "seconds. Once you place your bet, it will begin. " << endl <<" How much do you wager? " << endl << endl << endl; 
                         cin >> bet; 
                         
                         cout << "❂" << endl << endl << endl; 
                         
                        do
                            {
                           
                            diff = (clock() - start) / (double) (CLOCKS_PER_SEC);

                            }
                        while (diff < time); //credit to http://www.cplusplus.com/forum/beginner/226753/ for timer assistance   
                        }
                    cout << "Did you win?" << endl; 
                    cin >> accept; 
                    
                    if (accept == 'Y' || accept == 'y')
                        {
                        cout << "Incredible. You've passed the test. Take your earnings. " << endl; 
                                game = 1; 
                        }
                    else{
                        cout << "Frankly, we're just glad you survived. Take a consolidation prize anyway." << endl; 
                        bet = bet / 8; 
                        game = 1; 
                        }
                    }
            } //end of entire switch chain aka end of two games 
        if (game == 1)
            {
            cout << "Looks like you won... you earned " << bet * 2 << endl;
            money = moneyPoolAdd(money, bet);

            }
        else if (game == 0)
            {
            cout << "Looks like you lost!!! Awww, better luck next time. You lost " << bet << endl;
            money = moneyPoolSub(money, bet);

            }
        else
            {
            cout << "Go Ahead and try again." << endl;
            }

        cout << "Do you want to continue playing here? Enter Y if so!" << endl;
        cin >> confirm; //confirms if player wants to play again 
        int i = 0;
        while (i < 30)
            {
            cout << " " << endl;
            i++;
            }

        }//end of while loop 
    while (confirm == 'Y' || confirm == 'y'); // will loop back to beginning if player enters anything but Y 
    cout << "You ended with " << money << "!!" << endl;
    cout << "Thank you for playing! Please come again soon!" << endl;
    return 0;
    }
//reference to game win, as a function 

int gameWin(int game, int bet, int money)
    {

    return money;
    } 