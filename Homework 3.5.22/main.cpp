/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 5, 2022, 12:49 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //Problem 1: Example of a syntax error 
    
   // cout << var1 << "This is an example of cout text" << endl ; 
         //this would be a syntax error since the variable var1 was not declared as an integer 
    
    //example of a logic error 
        // dividing by zero would be a logical error
    /* 
     int var1 = 41
     *int var2= 0 
     * cout << "The variables divide to be" << var1 / var2 ; 
     
     */
    
    
    
    //Problem 2: Considering a snapshot
        //This snapshot is meant to be shown as a conversation between "Calvin" and "Bill"
    // for example, "Hello my name is Calvin," is followed by 
    // " Hi there, Calvin! My name is "Bill." 
    
    // b. There are 10 statements within the main function, if return 0; counts. 
    
    //c. The variable declaration would be 
    /* 
     string name1 = "calvin" ;
     * string name2 = "Bill" ;
     
     * 
     * the initialation statements would be 
     #include <cstdlib>
    #include <iostream>

    using namespace std;
     * 
     * 
     * d. The code that is in the program would be every statement that begins with "cout" for this particular snapshot
     * 
     * e. the inclusion of these two variables, while not necessary, makes it easier to change the names 
     * in the event that the programmer wants to. 
    
    */
    
    
    
    //problem 3....
    
    int john1 , john2 , john3 , john4;
    int mary1 , mary2, mary3 , mary4 ; 
    int matt1 , matt2, matt3 , matt4 ; 
    
    cout << "We're going to input test scores for John, Mary and Matt, and then average them. Please start with John's 4 test scores. " << endl ; 
    
    cin >> john1 >> john2 >> john3 >> john4; 
    
    cout << "Nice! Now for Mary..." <<endl; 
    
    cin >> mary1 >> mary2 >> mary3 >> mary4 ; 
    
    cout << "Alright! Last and least, Matt..." << endl; 
    
    cin >> matt1 >> matt2 >> matt3 >> matt4 ; 
    
    cout << "That's what we call 'epic.' Let's output that..." << endl << endl; 
    
    
    cout << left << setw(9) << "Name" << right << setw(9) << "Quiz 1" << setw(9) << "Quiz 2" << setw(9) << "Quiz 3" << setw(9) << "Quiz 4" <<endl ; 
    
     cout << left << setw(9) << "-----" << right << setw(9) << "-----" << setw(9) << "-----" << setw(9) << "-----" << setw(9) << "-----" << endl; 
    
    cout << left << setw(9) << "John" << right <<  setw(9) << john1 << setw(9) << john2 << setw(9) << john3 << setw(9) << john4 << endl; 
    
     cout << left << setw(9) << "Mary" << right << setw(9) << mary1 << setw(9) << mary2 << setw(9) << mary3 << setw(9) << mary4 << endl; 
    
     cout << left << setw(9) << "Matthew" << right << setw(9) << matt1 << setw(9) << matt2 << setw(9) << matt3 << setw(9) << matt4 << endl << endl ; 
     
     
      cout << left << fixed << setprecision(2) << setw(9) << "Average" << right << setw(9) << (double)(john1 + mary1 +matt1)/3 << setw(9) << (double)(john2 + mary2 + matt2)/3 << setw(9) ;
      cout << (double)(john3 + mary3 + matt3)/3 << setw(9) << (double)(john4 + mary4 + matt4)/3 << endl; 
     
     
    //wow that was something
      
     //problem 4
      
      string name ;
      string name2 ;
      string food ;
      string number; 
      string adj ;
      string color ;
      string animal ; 
     
      
      cout << "Hello! You seem like a fun person. Let's play a game of mad lib! Just follow my instructions." << endl ;
      cout << "first, enter a name. 1-10 characters please." << endl;
      
      cin >> name ; 
      cout << "Oh that's a fun name. Gimme another!" << endl; 
      
      cin >> name2; 
      cout << "Simply epic. Now, give me a food, still within 10 characters." << endl; 
      
      cin >> food ; 
      cout << "sounds delicious. How about a number?" << endl ; 
      
      cin >> number ; 
      cout << "My new lucky number. Now give me an adjective! " << endl ;
      
      cin >> adj ; 
      cout << "Getting there! Choose a color if you will. " << endl; 
      
      cin >> color ; 
      cout << "Alright! Last and least, an animal." << endl ; 
      
      cin >> animal ; 
      
      cout << "Excellent job. Let me process that...." << endl << endl << endl; 
      cout << "Dear, " << name << endl << " I am sorry that I am unable to turn in my homework at this time. First, I ate a rotten " << food ;  
      cout << " which made me turn " << color << " and extremely ill. I came down with a fever of " << number << ". Next, my " ; 
      cout << adj << " pet " << animal << endl << " must have smelled the remains of the " << food << " on my homework because he ate it. " ; 
      cout << " I am currently rewriting my homework and hope you will accept it late. " << endl << endl << "Sincerely" << endl << right << name2 ;

      
      
    return 0;
}

