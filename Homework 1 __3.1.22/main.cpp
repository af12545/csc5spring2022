/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 1, 2022, 3:54 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // Homework Problem 1 
    int num1 = 50;
    int num2 = 100; 
    
    int total = num1 + num2 ; 
    
    cout << "the total is: " << total << endl <<endl ;
    
    
    //homework problem 2 
    
    double taxState =0.04;
    double taxCounty = 0.02;
    double salePrice; 
    
    
    cout << "Please Enter Sale Price: " << endl ; 
    
    cin >> salePrice ; 
    double taxes = salePrice * (taxState + taxCounty) ; 
    double totalPrice = taxes + salePrice; 
   
    cout << totalPrice <<endl <<endl  ;
    
    
   //Restaurant Bill problem 3
    
    double taxPerc = .0675;  
    double initialBill; 
    double tipPerc;
 
    cout << "It's time to pay the bill. Input the cost here, first the price, then the tip: " <<endl ;
    cin >> initialBill ; 
    cout << endl ; 
    cin >> tipPerc; 
    
    double totalTax = (initialBill * taxPerc);
    double totalWithTax = totalTax + initialBill ;
    double tippedAmount = (totalWithTax * (tipPerc / 100));  
    double grandTotal = (tippedAmount + totalWithTax);
    
    cout << "The amount Tipped will be: "<< tippedAmount << endl; 
    
    cout << "The total is: " << grandTotal <<endl <<endl ; 
    
    
    //problem 4, Miles Per Gallon 
    
    double miles = 375 ;
    double gas = 15  ; 
    
    double milesPerGallon = miles / gas ;
    cout << "The Miles Per Gallon are: " <<milesPerGallon << endl << endl ; 
    
    // Problem 5, Personal Info 
    
  
    cout << "my name is Alex." << endl << "My number is: 911" << endl << "My college major is: Cybersecurity." << endl ; 
    
    //problem 6 
    

int maino;
int number_of_pods, peas_per_pod, total_peas;
cout << "Hello." <<endl ; 
cout << "Press return after entering a number. \n";
cout << "Enter the number of pods:\n";
cin >> number_of_pods;
cout << "Enter the number of peas in a pod:\n";
cin >> peas_per_pod;
total_peas = number_of_pods / peas_per_pod;
cout << "If you have ";
cout << number_of_pods;
cout << " pea pods\n";
cout << "and " ;
cout << peas_per_pod ;
cout <<" peas in each pod, then\n" ;
cout << "you have " ;
cout << total_peas ;
cout <<  " peas in all the pods.\n" << endl << "Goodbye." <<endl ;


// problem 6 part 5: custom problem. 

int var1 ;
int var2 ;

cout << "Give me two numbers. Any numbers. " ;
cin >> var1 >> var2 ; 
        

cout << "Here. " << var1 + var2 << endl ;
 

    return 0 ;


}
