/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 4, 2022, 11:42 AM
 */

#include <cstdlib>
#include <iostream> //dont forget to add iostream
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //VARIABLES 
    
    int x; //data_type name 
    
    //DECLARE AND INITIALIZE 
    
    string name = "dave" ; // "Dave" is a string literal 
    int y = 3; //
    
    
    //Output X to the user 
    cout << "x " << x << endl; //the default for x contains a junk value since we didn't assign it a value. It depends on the compiler. 
    
    
    //Multiple Declarations 
    double num1, num2, num3;
    num1 = num2 = num3 = 3.14; 
    
    //testing out the theory 
    cout << num1 ; 
    
    //CONST modifier (constant) 
    //constant variables should be in all caps
    const double PIE = 3.14 ; 
    //pie = 2.0;  //the const function prevents the value of pie being changed by accident 
    
    
    //Types of errors 
    // Syntax error: when the rules of the language are broken. 
    //ex int x --> forgetting semicolon 
    
    
    //logic error: is an error in the logic of the code 
    num1 = 3.0 ;
    num2 = 2.0 ;
    num3 = 5.0 ; 
    
    double avg = (num1 + num2 + num3) /3 ; //logic error 
    cout << "The average is: " << avg << endl; 
          
    //I/O input errors 
    
    string lastName;
   // cout << "Enter last name: "; 
   // cin >> lastName; //the stream operator stops at enter or space. 
   // cout << "Your last name: " << lastName << endl ; 
    
    //two more ways to get input 
    //want an entire sentence from the user? 
    //use getline function 
    cout << "Enter last name: "; 
    getline (cin, lastName); 
    cout << "your last name is " << lastName << endl; 
    
    //Reading a single character 
    char c; 
    cout << "enter last name: "; 
    cin.get(c); //this gets one single character.
    cout << "your last name is: " << c <<endl; 
    
    //conversion of data_types 
    //converison between doubles and ints 
    
    int val = 3.14; //implicit conversion 
    cout << "val: " << val; 
    
    //can only add/assign same data types 
    //how do we convert between data types?
    //Use static casting to cast a variable to a different type 
    //syntax: static_cast<double>(val_to_cast) 
    int numValues = 3; 
    
    //casting is not permanent 
    avg = (num1+num2+num3) /3 / static_cast<double>(numValues); 
    
    
    return 0;
}

