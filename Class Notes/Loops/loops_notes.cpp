/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   loops_notes.cpp
 * Author: itchy
 *
 * Created on March 8, 2022, 3:15 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Loops 
    
    /*There are 3 kinds of loops 
     *      while loop 
     *      do-while
     *      for loop
     *  OR LOOP
     *      syntax: 
     * for (iterator; conditional expression; iterator change) 
     * ex. iterator --> int i = 1 
     * ex. condition --> i < 10 (loop continues while condition is true) 
     * ex change --> i++ (this increments value by 1) 
     * 
     * step 1 only happens once. after step 4, it loops back to step 2 
     * 
     * 
     
     
     
     
     */  
    /* for (int i = 1 ; i <= 10 ; i++ )  // example of while loop, while i is 1, it satisfies the loop
        //in this case it will continue until it is greater than 10 
    {
        cout << i << endl; 
    }
    */
    //this is a more typical for loop 
   /* cout << "Typical for loop: " << endl; 
    for (int i = 0; i < 10 ; i++ ) 
    {
        cout << i << endl ;
    }
    /*
    //outputting a large amount of numbers
    /* for loops are used for counting / when we know the number of loops 
    for (int i=0 ; i < 100 ; i++ )
    {
        cout << i + 1 << endl ; 
    }
    */ 
    
    /* While loop
     * syntax: while (condition) 
     {
     code 
     }
     
     
     if you declare a variable within a loop, it only exists within the scope 
     */
    int i = 0;  // still need an iterator 
    //use while loops used for unknown # of loops (user input)  
    while ( i < 10)
    {
        cout << i << endl ; 
        i ++ ; //the position of the iterator affects the output 
    }
    
    // Do-while loop 
    /* 
     syntax: 
     * do 
     * {
     * code 
     * }
     * while ( conditional expression) ; 
     // do-while loops guarantee a single execution 
     
     
     use do while for user input (unkown # of loops) good for guaranteeing at least one instance
     */ 
    cout << "outputting do while loop" << endl; 
    i = 0; 
    do 
    {
        cout << i << endl; 
        i++ ; 
    }
    while (i < 10 ) ; 
    
    return 0;
}

