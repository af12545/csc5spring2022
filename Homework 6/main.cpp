/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on April 8, 2022, 10:08 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;
//overloaded function for max

double max(double input1, double input2) //parameters for problem one when only two variables are chosen. 
    {
    if (input1 > input2) //if statements determine which of the three numbers is larger and returns input to main accordingly 
        {
        return input1;
        }
    else
        {
        return input2;
        }

    }

double max(double input1, double input2, double input3) //parameters for problem 1, overloaded function is used here if 3 inputs are used 
    {
    if (input1 > input2 && input1 > input3) //if statements determine which of the three numbers is larger and returns input to main accordingly 
        {
        return input1;
        }
    else if (input2 > input1 && input2 > input3)
        {
        return input2;
        }
    else if (input3 > input1 && input3 > input2)
        {
        return input3;
        }
    else
        {
        cout << "You goofed up." << endl; // Returns this output in case there is a logic error during testing 
        }
    }

void swapIntegers(int ger1, int ger2) //this function inverts two integers
    {
    int gerTemp;
    cout << "Please Input 2 integers. " << endl;
    cin >> ger1 >> ger2; //gets input from user 

    gerTemp = ger1;
    ger1 = ger2;
    ger2 = gerTemp;
    cout << "Interger 1 is now: " << ger1 << endl << "Interger 2 is now: " << ger2 << endl;
    }

void swapStrings(string string1, string string2) //this function takes strings and swaps them using simple algebra 
    {
    string stringTemp;
    cout << "Please Input 1 word." << endl;
    cin >> string1;

    cout << "Now enter another." << endl;
    cin >> string2;

    stringTemp = string1;
    string1 = string2;
    string2 = stringTemp;

    cout << "String 1 is now: " << string1 << endl << "String 2 is now: " << string2 << endl; //output with no return to main          
    }

void swapChars(char char1, char char2) //this function swaps characters into strings 
    {
    string combine;
    string combine1;
    string combine2;

    cout << "Enter a letter!" << endl;
    cin >> char1;
    cout << "Enter one more please!" << endl;
    cin >> char2;


    combine += char1; //uses operators to add characters into the string combine, since characters are overloaded functions when it comes to 
    combine += char2;

    cout << "Your strings are now: " << combine << "!" << endl;

    //2nd part of character swap

    cout << "Alternatively..." << endl;
    combine1 = char1; //alternative method for two strings, one and two characters using the more basic = operand 
    combine2 = char2;

    cout << "Character 1: " << combine1 << endl << " Charater 2: " << combine2 << endl;

    }

double inputFeet(double feet) //declares parameters for feet
    {
    cin >> feet; //takes input for feet
    return feet; //returns feet to main
    }

double inputInches(double inches) //declares parameters for inches 
    {
    cin >> inches; //takes input for inches
    return inches; // returns inches to main 
    }

double convFeet(double feet, double convMeters, double meters) //declares paramerters for feet, conversion value for meters, and a variable for the output to be stored in 
    {
    meters = feet * convMeters; //converts the feet using the conversion value to meters 
    return meters; //returns meters value to main function 
    }

double convInches(double inches, double convCentimeters, double centimeters) //declares parameters for inches, the conversion value for centimeters, and a variable for the output to be stored in 
    {
    centimeters = inches * convCentimeters; //converts to centimeters using the conversion value times inches input 
    return centimeters; //returns gram value to main function
    }

double finalPut(double meters, double centimeters) //declares parameters from the previous function for use in this function 
    {
    cout << "You have " << meters << " meters and " << centimeters << " centimeters." << endl; //outputs a cout statement from the declared parameters, ending the program 
    }

void computeCoin(int coinVal, int & num, int & amountLeft);

/*
 * 
 */
int main(int argc, char** argv)
    {
    //problem 1: Write two overloaded functions called max that takes either two or three parameters of type 
    //double and returns the largest of them.

    int switchinput;
    int chooseProgram;
    cout << "Which Program do you wish to run?" << endl << "Program 1: 2 or 3 on an overloaded function" << endl << "Program 2: Swap integers" << endl << "Program 3: Swap Strings " << endl;
    cout << "Program 4: Char function" << endl << "Program 5: Feet and inches" << endl << "Program 6: Coins" << endl << endl;
    cin >> chooseProgram;

    switch (chooseProgram)
        {
        case 1: // Program 1 
        {

            cout << "Give me 2 or 3 numbers into this overloaded function!" << endl;
            cout << "2: Enter 2 for two numbers" << endl << "3: Enter 3 for 3 numbers." << endl << endl;
            cin >> switchinput;

            switch (switchinput)
                {
                    double input1, input2, input3;
                case 2:
                {
                    cin >> input1 >> input2;
                    cout << "The highest input was: " << max(input1, input2, input3) << "!" << endl;
                }
                case 3:
                {
                    cin >> input1 >> input2 >> input3;
                    cout << "The highest input was: " << max(input1, input2, input3) << "!" << endl;
                    break;
                }

                }
            break;
        } // end of program 1 

        case 2: // Program 2, swaps integers 
        {
            int ger1, ger2;
            swapIntegers(ger1, ger2);
            break;
        }
        case 3: //Program 3, swaps strings 
        {
            string string1, string2;
            swapStrings(string1, string2);
            break;
        }
        case 4: //Program 4, swaps characters into strings 
        {
            char char1, char2;
            swapChars(char1, char2);
            break;
        }
        case 5: //Problem 5, converting feet to inches 
        {
            double feet;
            double inches;
            double convMeters = 0.3048;
            double convCentimeters = 2.54;
            double meters;
            double centimeters;
            cout << "This program will convert feet and inches to meters and centimeters! Please enter your measurement, feet first, inches after." << endl;

            feet = inputFeet(feet);
            inches = inputInches(inches);
            cout << "You entered " << feet << " feet and " << inches << " inches." << endl;

            meters = convFeet(convMeters, feet, meters);
            centimeters = convInches(convCentimeters, inches, centimeters);

            finalPut(meters, centimeters);
            break;
        }
        case 6: //Problem 6, using function to divide coins 

            int change;
            int num;
            char choice;



            do
                {

                cout << "Input your change from 1 cent to 99 cents!";
                cin >> change;
                if (change = < 1 && change > 100)
                    {
                    cout << change << endl << "The most efficient breakdown is as follows; " << endl;
                    computeCoin(25, num, change);
                    if (num)
                        {
                        cout << num << " Quarter/s " << endl;
                        }
                    computeCoin(10, num, change);

                    if (num)
                        {
                        cout << num << " Dime/s " << endl;
                        cout << "And lastly, " << change << " penny/pennies. " << endl;
                        }
                    else if (change)
                        {
                        cout << "And lastly, " << change << "penny/pennies. " << endl;
                        }
                    else
                        {
                        cout << "Hmm what's going on?" << endl;
                        }
                    }
                else
                    {
                    cout << "Try that again. Has to be between 0 and 100 cents!" << endl;
                    }
                cout << endl << "Do you want to calculate again?" << endl << "Y: Calculate Again" << endl << "N: End Program" << endl;
                cin >> choice;
                }
            while (choice == 'Y' || choice == 'y');
        } // end of main switch 


    return 0;
    }

void computeCoin(int coinValue, int& number, int& amountLeft) //this function uses reference parameters to reuse the calculation within to be used for quarters and dimes. 
    {
    number = amountLeft / coinValue;
    amountLeft = amountLeft % coinValue;
    }
