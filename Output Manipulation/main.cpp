/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 4, 2022, 12:20 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip> //supports ALL manipulation

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    cout <<"Hello World" << endl; 
    
    double num = 3.123456; 
    cout << "Num:" << num <<endl; 
    //setprecision 
    //sets the number of maximum digits we want in output 
    
    cout << setprecision(2) << num << endl;  //sets a maximum number of digits that are presented 
    
    double num2 =3.045237 ; //setprecision is a permanent modifier, is is affecting num2 just like num 
    cout <<  setprecision(4) << num2 << endl; 
    
    
    //we combine setprecision with fixed 
    // when using fixed with setprecision, setprecision now refers to decimals 
    cout << fixed << setprecision(5) <<num2 <<endl; 
    
    //typically for currency, we use setprecision(2) ; 
    cout << setprecision(2) << "total amount of money: " << num2 << endl ; 
    
    double num3 = 5.8612 ; 
    
    //Use setw(num of characters) to create a set column width 
    //setw is NOT a persistent modifier aka does not  work except for this next output 
    //Add Headers
    cout << setw(10) << "USD" << setw(10) << "EU" << setw(10) << "pesos" << endl ;
    cout << setw(10) << num <<setw(10) << num2 << setw(10) << num3 << endl; 
    //outputs each number with 10 characters dedicated to each variable 
    
    // justification 
    //use left or right to change the justification 
    //left/right is a persistent modifier 
    
    cout << left <<setw(10) << num2 << setw(10) << num3 << endl; 
    
    //setfill 
    // affects all setw that are used
    //fills in any blank space from setw 
    //pass in a single character that you want to fill
    cout << setfill('*') ; 
     cout << setw(10) << "USD" << setw(10) << "EU" << setw(10) << "pesos" << endl ;
    cout << setw(10) << num <<setw(10) << num2 << setw(10) << num3 << endl; 
    
    
    cout << setfill('O') << setw(80) << "" << endl ; 
    
    
    
    
    
    return 0;
}

