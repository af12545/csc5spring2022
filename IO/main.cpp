/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on February 25, 2022, 1:45 PM
 */
// make sure to include iostream in just about every project that includes any IO
#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //what is IO?
    //IO stands for input/output
    
    
    //writing something to the console 
    //syntax; 
    //cout << something_to_output 
    //use \n (new line character)
    cout << "hello world\n" <<endl; //output the string hello world to the console 

    //endl is a force newline output 
    cout << "My name is: " << 20 << " hello?" << endl << endl 
            << " world" << 3.14 << endl; 
    
    
    //declare and initialize a variable 
    int numberOfCars = 20; 
    // Use a stream operator (<<) when data type changes ex. string to integer 
    cout << "number of cars: " << numberOfCars << endl; 
    //the stream operator acts as an extension of the cout command. 
    
    
    // INPUT 
    //we can get input from the console or file 
    // to get input, you need to store data from the user into a variable 
    
    
   //ex. get a name or address from a user 
    string name; 
    
    
    //Make sure to PROMPT the user!!
   cout << "please enter a name: " ;
    //User cin for input, like how cout is for output 
    cin >> name; 
            
         //Output the name back to the user
    
    cout << "your name is "  << name << endl ; 
            
    
    //get 3 numbers from the user 
    cout << "please enter 3 numbers: ";
    
    //now you need 3 variables to store 3 numbers 
    
    int num1, num2, num3; 
    cin >> num1 >> num2 >> num3; 
    
    //output to the user 
    cout << "Your 3 numbers were: " << num1 << num2 <<num3 <<endl; 
    
    return 0;
}

