/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: itchy
 *
 * Created on March 4, 2022, 3:41 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // demorgan's law applies to boolian logic 
    /* 
     
     
     
     
     */
    bool expl = 1 > 2; //false
    bool exp2 = 4==4 ; //true
    bool exp3 = 3 < 10; //true 
    bool exp4 = 6 != 6; //false 
    
    //false && false 
    // exp1 && exp4 
   // if (exp1 && exp4 ); 
   // if (1 > 2 && 6 != 6);
    
    // !(false && false) 
    // !(false) 
    //means true 
    //basically the distributive property across the variables
    
    //(false && false)) ex. 2 * ( 3+4) ==> 2*3 + 2*4
    // !false !&& !false // !&& => ||.......!|| => &&
    // true || true
    // true
    
    // !true && !(false || !(true)) 
    // ! (true && !(false || false)) 
    //!(true && !(false) 
    //means true and true 
    //false || (false || false) 
    //false 
    
    if (exp1 && 1(exp2 || exp3) || exp4)
    {
        cout << "True" <<endl; 
    }
    
    else 
    { <<cout << "false" << endl; }
    
    //! (exp 1 &&  !(exp2 || exp3) || exp4)
    // exp1 !&& !!(exp2 || exp3) !|| exp4) 
    // exp1 || (exp2 || exp3) && exp4 
    
    if (!exp1 || (exp2 || exp3) && exp4)
    {
        cout << "True" << endl; 
    }
    else 
    {
        cout << "false" << endl; 
                
    }
    return 0;
}

